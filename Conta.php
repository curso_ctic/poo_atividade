<?php 


abstract class Conta{
    
    private $nome;
    private $login;
    private $senha;
    private $saldo;
    private $tipoConta;
    

    public function __construct($nome,$login,$senha){
        self::setNome($nome);
        self::setLogin($login);
        self::setSenha($senha);
    }
    
    
    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    
    public function getTipoConta()
    {
        return $this->tipoConta;
    }
    
    /**
     * @param mixed $tipoConta
     */
    public function setTipoConta($tipoConta) {
        $this->tipoConta = $tipoConta;
    }
    
    abstract public function sacar($valor);
    
    abstract public function depositar($valor);
    
    abstract public function mostrarInformacoes();

}

?>