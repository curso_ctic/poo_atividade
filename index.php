
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Fundamentos básicos com PHP</title>
    <link rel="stylesheet" href="style.css" media="all" />

</head>
<body style="background-color: white;">
	<?php 
       
	include_once './Aluno.php';
	
	
	$aluno = new Aluno("Eron", "Eron10","1234","201406840037","Engenharia da Computação");
	
	$aluno->mostrarInformacoes();
	
	$aluno->sacar(10);
	$aluno->mostrarInformacoes();

// 	include_once './Servidor.php';
	
// 	$servidor = new Servidor("Eron", "Eron10","1234","201407","Gerente");
	
// 	$servidor->mostrarInformacoes();
// 	$servidor->sacar(30);
// 	$servidor->mostrarInformacoes();
// 	$servidor->depositar(40);
// 	$servidor->mostrarInformacoes();
	
	?>

</body>
</html>
