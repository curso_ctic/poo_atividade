<?php 
include_once './Conta.php';

class Servidor extends Conta{
    
    private $codigo;
    private $cargo;
    
    public function __construct($nome, $login, $senha,$codigo,$cargo){
        parent::__construct($nome, $login, $senha);
        self::setCodigo($codigo);
        self::setCargo($cargo);
        parent::setTipoConta("Servidor");
        parent::setSaldo(50);
}
    /**
     * @return mixed
     */
    public final function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return mixed
     */
    public final function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param mixed $codigo
     */
    public final function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @param mixed $cargo
     */
    public final function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    public function sacar($valor){
        $porcentagem = ($valor* 1)/100;
        parent::setSaldo(parent::getSaldo() - ( $valor + $porcentagem));
    }
    
    public function depositar($valor){
        $porcentagem = ($valor* 1)/100;
        parent::setSaldo(parent::getSaldo() + ($valor - $porcentagem));
    }

    public function mostrarInformacoes(){
        echo "<table style='width:20%''>
              <tr>
                  <th>Nome</th>
                  <th>Login</th>
                  <th>Senha</th>
                  <th>Saldo</th>
                  <th>TipoConta</th>
                  <th>Codigo</th>
                  <th>Cargo</th>
              </tr>
              ";
        
        echo "<tr>
                 <td>".parent::getNome()."</td>
                 <td>".parent::getLogin()."</td>
                 <td>".parent::getSenha()."</td>
                 <td>R$ ".parent::getSaldo()."</td>
                 <td>".parent::getTipoConta()."</td>
                 <td>".self::getCodigo()."</td>
                 <td>".self::getCargo()."</td>
              </tr>";
    }
    


}

?>