<?php 

include_once './Conta.php';

class Aluno extends Conta{
    
    private $matricula;
    private $curso;
    
    
    public function __construct($nome,$login,$senha,$matricula,$curso){
        parent::__construct($nome,$login,$senha);
        self::setMatricula($matricula);
        self::setCurso($curso);
        parent::setSaldo(25);
        parent::setTipoConta("Estudante");
    }
    
    /**
     * @return mixed
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * @return mixed
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param mixed $matricula
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
    }

    /**
     * @param mixed $instituicao
     */
    public function setcurso($curso)
    {
        $this->curso = $curso;
    }
    
    public function sacar($valor){
        $porcentagem = ($valor* 0.5)/100; 
        parent::setSaldo(parent::getSaldo() - ( $valor + $porcentagem));
    }
    
    public function depositar($valor){
        $porcentagem = ($valor* 0.5)/100; 
        parent::setSaldo(parent::getSaldo() + ($valor - $porcentagem)); 
    }
    
    
    public function mostrarInformacoes(){
        echo "<table style='width:20%''>
              <tr>
                  <th>Nome</th>
                  <th>Login</th>
                  <th>Senha</th>
                  <th>Saldo</th>
                  <th>TipoConta</th>
                  <th>Matricula</th>
                  <th>Curso</th>                  
              </tr>
              ";
        
        echo "<tr>
                 <td>".parent::getNome()."</td>
                 <td>".parent::getLogin()."</td>
                 <td>".parent::getSenha()."</td>
                 <td>R$ ".parent::getSaldo()."</td>
                 <td>".parent::getTipoConta()."</td>
                 <td>".self::getMatricula()."</td>
                 <td>".self::getCurso()."</td>
              </tr>";
    }
    
}




?>